/* global $*/
var baseUrl = 'https://rest.ehrscape.com/rest/v1';
var queryUrl = baseUrl + '/query';
var username = "ois.seminar";
var password = "ois4fri";
var trenutniEHR = [];


function kreirajEHR(ime, priimek, datumRojstva, callback) {
		$.ajax({
      url: baseUrl + "/ehr",
      type: 'POST', 
      headers: {
        "Authorization": "Basic " + btoa(username + ":" + password)
      },
      success: function (data) {
        var ehrId = data.ehrId;
        var partyData = { firstNames: ime, lastNames: priimek, dateOfBirth: datumRojstva, additionalInfo: {"ehrId": ehrId}};
        $.ajax({url: baseUrl + "/demographics/party", type: 'POST',
          headers: {
            "Authorization": "Basic " + btoa(username + ":" + password)
          },
          contentType: 'application/json',
          data: JSON.stringify(partyData),
          success: function (party) {
            if(ime=="Tone") {
              trenutniEHR[0] = ehrId;
            } else if(ime=="Tina") {
              trenutniEHR[1] = ehrId;
            } else if(ime=="Freddie") {
              trenutniEHR[2] = ehrId;
            }
              console.log(ehrId);
              $("#kreirajSporocilo").html("<span class='obvestilo " +
                "label label-success fade-in'>Uspešno kreiran EHR: "+ehrId+".</span>");
              $("#Oglas").append('<button type="button" class="btn btn-primary btn-block" data-toggle="collapse" data-target="#'+ehrId+'">'
              + ime+" "+priimek+' • '+datumRojstva+' • '+ehrId+'</button>'
              + '<div id="'+ehrId+'" class="collapse"><hr>'+party.meta.href+'<hr>'
              + '<table id="tab'+ehrId+'" class="table table-hover"><tr><th>Datum in ura</th><th>Teza</th><th>Visina</th><th>Temperatura</th></tr></table>'
              + '<button type="button" class="btn btn-danger btn-block" data-toggle="collapse" data-target="#graf'+ehrId+'">Izriši podatke</button>'
              +'<div id="graf'+ehrId+'" style="min-height:400px" class="collapse"></div></div>');
                ustvariGraf(ehrId);
              },
          error: function(err) {
          	$("#kreirajSporocilo").html("<span class='obvestilo label " +
              "label-danger fade-in'>Napaka '" +
              JSON.parse(err.responseText).userMessage + "!");
          }
        });
      }
  	});
  }
  
  
  function ustvariGraf(ehrId) {
    $.ajax({
   	  url: baseUrl + "/demographics/ehr/" + ehrId + "/party",
    	type: 'GET',
    	headers: {"Authorization": "Basic " + btoa(username + ":" + password)},
    	success: function (data) {
				var party = data.party;
				$.ajax({
				  url: baseUrl + "/view/" + ehrId + "/" + "weight",
			    type: 'GET',
			    headers: {"Authorization": "Basic " + btoa(username + ":" + password)},
  			    success: function (res) {
  			    	if (res.length > 1) {
  			    	   var podTeze = [];
    			    	  for(var i = res.length-1; i >= 0; i--) {
    			    	    podTeze.push({
                      label: i,
                      value: res[i].weight
                      });
    			    	    }
    			    	     $("#graf"+ehrId).insertFusionCharts({
                      type: "line",
                      width: "100%",
                      height: "80%",
                      dataFormat: "json",
                      dataSource: {
                        chart: {
                          caption: "Spreminjanje teže skozi čas",
                          yaxisname: "Teža (Kg)",
                          numbersuffix: " Kg",
                          rotatelabels: "1",
                          setadaptiveymin: "1",
                          theme: "fusion"
                        },
                        data: podTeze
                      }
                    });
				        
  			    	} else {
  			    		  $("#graf"+ehrId).html('<div class="alert alert-danger"><strong>Graf se bo prikazal, ko vnesete vsaj 2 podatka o teži posameznika!</strong></div>');
  			    	}
  			    }
				});
    	}
		});
  }

function dodajMeritve(ehrId, datumInUra, telesnaVisina, telesnaTeza, telesnaTemperatura, 
sistolicniKrvniTlak, diastolicniKrvniTlak, nasicenostKrviSKisikom, merilec) {
		var podatki = {
			// Struktura predloge je na voljo na naslednjem spletnem naslovu:
      // https://rest.ehrscape.com/rest/v1/template/Vital%20Signs/example
		    "ctx/language": "en",
		    "ctx/territory": "SI",
		    "ctx/time": datumInUra,
		    "vital_signs/height_length/any_event/body_height_length": telesnaVisina,
		    "vital_signs/body_weight/any_event/body_weight": telesnaTeza,
		   	"vital_signs/body_temperature/any_event/temperature|magnitude": telesnaTemperatura,
		    "vital_signs/body_temperature/any_event/temperature|unit": "°C",
		    "vital_signs/blood_pressure/any_event/systolic": sistolicniKrvniTlak,
		    "vital_signs/blood_pressure/any_event/diastolic": diastolicniKrvniTlak,
		    "vital_signs/indirect_oximetry:0/spo2|numerator": nasicenostKrviSKisikom
		};
		var parametriZahteve = {
		    ehrId: ehrId,
		    templateId: 'Vital Signs',
		    format: 'FLAT',
		    committer: merilec
		};
		$.ajax({
      url: baseUrl + "/composition?" + $.param(parametriZahteve),
      type: 'POST',
      contentType: 'application/json',
      data: JSON.stringify(podatki),
      headers: {
        "Authorization": "Basic " + btoa(username + ":" + password)
      },
      success: function (res) {
        $("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-success fade-in'>" +
          res.meta.href + ".</span>");
          //console.log("tab"+ehrId);
        	$("#tab"+ehrId).append('<tr><td>' + datumInUra +'</td><td>' + telesnaTeza
                      +'</td><td>'+telesnaVisina+'</td><td>'+telesnaTemperatura+'</td></tr>');
        ustvariGraf(ehrId);
      },
      error: function(err) {
      	$("#dodajMeritveVitalnihZnakovSporocilo").html(
          "<span class='obvestilo label label-danger fade-in'>Napaka '" +
          JSON.parse(err.responseText).userMessage + "'!");
      }
		});
		if (ehrId != trenutniEHR[0] && ehrId != trenutniEHR[1] && ehrId != trenutniEHR[2]) {
		  //console.log(trenutniEHR);
		  ustvariOglas(ehrId);
		}
}


$(document).ready(function() {
  $(" #Prijava ").click(function() {
    var ime = $("#Ime").val();
	  var priimek = $("#Priimek").val();
    var datumRojstva = $("#DatumRojstva").val();
  
	  if (!ime || !priimek || !datumRojstva || ime.trim().length == 0 ||
      priimek.trim().length == 0 || datumRojstva.trim().length == 0) {
		$("#kreirajSporocilo").html("<span class='obvestilo label " +
      "label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
    } else {
      kreirajEHR(ime, priimek, datumRojstva, "");
    }
  })
  
  $("#dodajMeritve").click(function() {
    var ehrId = $("#dodajVitalnoEHR").val();
  	var datumInUra = $("#dodajVitalnoDatumInUra").val();
  	var telesnaVisina = $("#dodajVitalnoTelesnaVisina").val();
  	var telesnaTeza = $("#dodajVitalnoTelesnaTeza").val();
  	var telesnaTemperatura = $("#dodajVitalnoTelesnaTemperatura").val();
  	var sistolicniKrvniTlak = $("#dodajVitalnoKrvniTlakSistolicni").val();
  	var diastolicniKrvniTlak = $("#dodajVitalnoKrvniTlakDiastolicni").val();
  	var nasicenostKrviSKisikom = $("#dodajVitalnoNasicenostKrviSKisikom").val();
  	var merilec = $("#dodajVitalnoMerilec").val();
  	
  	if (!ehrId || ehrId.trim().length == 0) {
		$("#dodajMeritveVitalnihZnakovSporocilo").html("<span class='obvestilo " +
      "label label-warning fade-in'>Prosim vnesite zahtevane podatke!</span>");
  	} else {
  	  dodajMeritve(ehrId, datumInUra, telesnaVisina, telesnaTeza, telesnaTemperatura, 
  	  sistolicniKrvniTlak, diastolicniKrvniTlak, nasicenostKrviSKisikom, merilec);
  	}
  });
  
  
  $("#GeneriranjePodatkov").click(function() {
    $("#myModal").modal();
  });
  
  $("#UstvariOs").click(function() {
    if($("#1").is(":checked")) {
      kreirajEHR("Tone", "Cvetinski", "1969-10-24");
    }
     if($("#2").is(":checked")) {
        kreirajEHR("Tina", "Turner", "2007-03-10");
    }
     if($("#3").is(":checked")) {
        kreirajEHR("Freddie", "Mercury", "1982-06-31");
    }
    //console.log(trenutniEHR);
    setTimeout(function(){
          //console.log(trenutniEHR);atum in ura
      if($("#1").is(":checked")) {
        dodajMeritve(trenutniEHR[0], "2012-11-21T12:12Z", "182", "76.2", "36.5", "120", "95", "98", "");
        dodajMeritve(trenutniEHR[0], "2014-08-21T08:55Z", "182", "76.4", "36.4", "121", "92", "93", "");
        dodajMeritve(trenutniEHR[0], "2016-12-19T12:10Z", "183", "79.2", "36.5", "122", "91", "95", "");
        dodajMeritve(trenutniEHR[0], "2018-05-21T09:27Z", "184", "81.6", "37.6", "121", "96", "97", "");
        dodajMeritve(trenutniEHR[0], "2018-05-28T09:19Z", "185", "81.5", "38.3", "121", "98", "99", "");
        dodajMeritve(trenutniEHR[0], "2018-06-01T11:29Z", "185", "80.2", "37.2", "123", "96", "96", "");
      }
      if($("#2").is(":checked")) {
        dodajMeritve(trenutniEHR[1], "2017-06-30T07:04Z", "150", "48", "36.2", "118", "92", "98", "");
        dodajMeritve(trenutniEHR[1], "2018-02-12T11:23Z", "157", "54", "35.8", "120", "91", "96", "");
        dodajMeritve(trenutniEHR[1], "2018-03-03T13:40Z", "162", "62", "35.2", "121", "95", "99", "");
      }
      if($("#3").is(":checked")) {
        dodajMeritve(trenutniEHR[2], "2012-01-11T08:21Z", "152", "69", "35.8", "112", "97", "96", "");
        dodajMeritve(trenutniEHR[2], "2013-02-19T17:57Z", "152", "71", "35.4", "115", "96", "92", "");
        dodajMeritve(trenutniEHR[2], "2014-10-18T15:51Z", "152", "73", "36.1", "111", "98", "93", "");
        dodajMeritve(trenutniEHR[2], "2014-11-12T12:12Z", "152", "78", "37.2", "118", "97", "96", "");
        dodajMeritve(trenutniEHR[2], "2015-05-03T11:41Z", "152", "72", "35.2", "119", "96", "97", "");
        dodajMeritve(trenutniEHR[2], "2016-06-02T08:09Z", "152", "68", "37.8", "120", "95", "93", "");
        dodajMeritve(trenutniEHR[2], "2017-12-29T07:08Z", "152", "68", "35.7", "121", "97", "92", "");
        dodajMeritve(trenutniEHR[2], "2018-08-25T11:36Z", "152", "72", "35.9", "122", "98", "94", "");
      }
    }, 2000);
  });
});
  































  //USTVARI OGLAS
  var oglasi = ["Protein Bar", "Tropical Holidays", "Pizza", "Gym Card", "Healthy Dinner in a Restaurant", "Milka Chocolate", "McDonalds Burger"];
  var cene = [1.30, 1999.99, 6.49, 29.99, 21.00, 2.10, 3.30];
  
  function ustvariOglas(ehrId) {
    var teze = [];
    var visine = [];
    var koefVremena = 1;
    var bmi;
      	
    //PRIREDITEV GLEDE NA VREME
    $.getJSON("https://api.openweathermap.org/data/2.5/weather?q=Ljubljana&appid=2c1212ee95844bd26d545886e6f97e1f&units=metric",
    function(data) {
      if(data.weather[0].main == "Clear sky" || data.weather[0].main == "Few clouds") koefVremena += 0.1;
      if(data.weather[0].main == "Snow" || data.weather[0].main == "Thunderstorm" || data.weather[0].main == "Rain") koefVremena -= 0.1;
      if(parseFloat(data.main.temp_max) < 30.0 && parseFloat(data.main.temp_min) > 15.0) koefVremena += 0.1;
      if(parseFloat(data.main.temp) > 18.0 && parseFloat(data.main.temp) < 25.0) koefVremena += 0.5;
      if(parseFloat(data.main.temp) > 35.0 && parseFloat(data.main.temp) < 5.0) koefVremena -= 0.5;
    });
    
    //PRIREDITEV GLEDE NA PODATKE POSAMEZNIKA
    $.ajax({
      url: baseUrl + "/view/" + ehrId + "/" + "weight",
      type: 'GET',
      headers: {"Authorization": "Basic " + btoa(username + ":" + password)},
      success: function (res) {
        for (var i in res) {
          teze[i] = res[i].weight;
        }
      }
    });
	
    $.ajax({
      url: baseUrl + "/view/" + ehrId + "/" + "height",
      type: 'GET',
      headers: {"Authorization": "Basic " + btoa(username + ":" + password)},
      success: function (res) {
        for (var i in res) {
          visine[i] = res[i].height;
        }
    	}
    });
    
  	var oseba = "oseba";
  	if(teze.length && visine.length > 0) {
  	  if(visine[visine.length-1] > 150) {
  	    if(teze[teze.length-1]/(visine[visine.length-1]*visine[visine.length-1]) > 24,9) oseba="pretezko";
  	    else if(teze[teze.length-1]/(visine[visine.length-1]*visine[visine.length-1]) < 18.5) oseba="prelahko";
    } else {
      oseba = "otrok";
    }
	}
	
  var randomNum = Math.floor(Math.random() * 10) + 0;
	if (oseba == "oseba" || oseba == "otrok") var imeOglasa = Math.floor(Math.random() * 6) + 0;
	if (oseba == "prelahko") var imeOglasa = Math.floor(Math.random() * 4) + 0;
	if (oseba == "pretezko") var imeOglasa = Math.floor(Math.random() * 4) + 2;
	var key = "AIzaSyDO1RWSzzfVNVZBezE4eZbF7pZ3OLnE4gk";
	var cx = "017154400492333421262:67dihgn_xvq";
		//AIzaSyDO1RWSzzfVNVZBezE4eZbF7pZ3OLnE4gk
	var q = (oglasi[imeOglasa]).split(' ').join('+');
	$.getJSON( "https://www.googleapis.com/customsearch/v1?key="+key+"&cx="+cx+"&q="+q+"&searchType=image&fileType=jpg&imgSize=xlarge&alt=json", 
   function (data) {
    $(" #OglasOpis ").html("<h2><strong>"+ oglasi[imeOglasa] + "<hr><strong></h2><br>");
   $(" #PrikazOglasa ").html('<img src="' + data.items[randomNum].link + '" '+'height="250px" width="100%">');
    $(" #OglasOpis ").append("<h2>For only " + (koefVremena*cene[imeOglasa]).toFixed(2) + " €!</h2>");
    setTimeout(function(){
      //console.log(teze[teze.length-1], visine[visine.length-1]);
      bmi = (teze[teze.length-1]/((visine[visine.length-1]/100)*(visine[visine.length-1]/100))).toFixed(2);
      if(isNaN(bmi) == true) bmi = 24.39;
    $(" #OglasStat ").html('<hr><button class="btn btn-primary btn-block"><small>Koeficient vremena: '
    +koefVremena+" • BMI: "+bmi+'</small></button><br>');
    }, 2000);
  });
}





//VREME
window.addEventListener("load", function() {
  
  $.getJSON("https://api.openweathermap.org/data/2.5/weather?q=Ljubljana&appid=2c1212ee95844bd26d545886e6f97e1f&units=metric",
  function(data) {
    
    //PRIKAZOVANJE VREMENA
    //console.log(data);
    var time = new Date();
    var cas = time.toLocaleString('en-US', { hour: 'numeric', hour12: true })
    var ikona = "https://openweathermap.org/img/w/"+ data.weather[0].icon+".png";
    $(" #VremeHead ").html("<h2><strong>" + data.name + ", "+data.sys.country +"</strong></h2>");
    $(" #VremeHead ").append("<small>"+ fncDayOfWeek()+", "+cas+" • "+data.weather[0].main+"</small>");
    $(" #Vreme ").append($('<img>',{id:'theImg',src: ikona}).width(80).height(80));
    $(" #VremeDet ").html('<hr><button class="btn btn-primary btn-block"><small>Temperature: '+data.main.temp+
    " °C • Moisture: "+data.main.humidity+"% • Pressure: "+data.main.pressure+" hPa</small></button><br>  ");
  });
  function fncDayOfWeek() { 
    var stDate; 
    var now; 
    var assoc= new Array(7); 
    assoc[0]="Sunday"; 
    assoc[1]="Monday"; 
    assoc[2]="Tuesday"; 
    assoc[3]="Wednesday"; 
    assoc[4]="Thursday"; 
    assoc[5]="Friday"; 
    assoc[6]="Saturday"; 
    now= new Date(); 
    stDate=now.getDay(); 
    return(assoc[stDate]);
  }
});























































//IMPLEMENTACIJA ZEMLJEVIDA
window.addEventListener('load', function () {
  if (window.location.pathname.split(/(\\|\/)/g).pop() == "bolnisnice.html") {
    
  var L = window.L;
  var mymap = L.map('mapid').setView([46.056946, 14.505751], 14);
  
  L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
  attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
  maxZoom: 18,
  id: 'mapbox.streets',
  accessToken: 'pk.eyJ1IjoidGs1MjcyIiwiYSI6ImNqdmgxMTdsbTBkbWczeXA2Nmg5MDBxdnUifQ.hePLgheP5UN3QtGjner7Xg'}).addTo(mymap);
  
  var markerji = [];
  var zgradbe = [];
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-icon-2x-red.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });
  var trenutno;
  var zeleneZgradbe = [];
  
  
  function modreZgradbe() {
    $.getJSON('https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json', function(data) {
      for(var i = 0; i < data.features.length; i++) { //data.features.length
        var koordinate = [];
        //console.log(data);
        if (data.features[i].geometry.coordinates[0].length > 2) {
          for(var k = 0; k < data.features[i].geometry.coordinates[0].length; k++) {
            koordinate[k] = data.features[i].geometry.coordinates[0][k];
            var zamenjava = koordinate[k][0];
            koordinate[k][0] = koordinate[k][1];
            koordinate[k][1] = zamenjava;
          }
          //console.log(data.features[i].properties["addr:street"]);
          zgradbe[i] = koordinate;
          markerji[i] = L.polygon(koordinate).addTo(mymap);
          markerji[i].bindPopup(data.features[i].properties.name
          +"<br>"+data.features[i].properties["addr:street"]+" "+
          data.features[i].properties["addr:housenumber"]+ ", " +
          data.features[i].properties["addr:city"]);
        }
      }
    });
  }
  
  modreZgradbe();
  //console.log(zgradbe);
  
  mymap.on('click', function(e) {
    if (trenutno != null) mymap.removeLayer(trenutno);
      trenutno = L.marker([e.latlng.lat, e.latlng.lng], 
      {icon: ikona}).addTo(mymap);
      obarvanje(e.latlng.lat, e.latlng.lng);
  });
    
  function obarvanje(lng, lat) {
    for(var i = 0; i < zeleneZgradbe.length; i++) {
      if (zeleneZgradbe[i] != null) {
        mymap.removeLayer(zeleneZgradbe[i]);
        //console.log(zgradbe[i]);
        //var barvaNazaj = L.polygon(zgradbe[i]).addTo(mymap);
        //barvaNazaj.bindPopup(data.features[i].properties.name
      }
    }
    
    for (var j=0; j < zgradbe.length; j++) {
      if (distance(zgradbe[j][0][0], zgradbe[j][0][1], lng, lat, "K") <=5) {
        obarvajZeleno(j);
      }
    }
  }
  
  
  function obarvajZeleno(j) {
    $.getJSON('https://teaching.lavbic.net/cdn/OIS/DN3/bolnisnice.json', function(data) {
      if (data.features[j].geometry.coordinates[0].length > 2) {
        zeleneZgradbe[j] = L.polygon(zgradbe[j], 
        {color:"green",fillColor:"#90EE90"}).addTo(mymap);
        zeleneZgradbe[j].bindPopup(data.features[j].properties.name
        +"<br>"+data.features[j].properties["addr:street"]+" "+
        data.features[j].properties["addr:housenumber"]+ ", " +
        data.features[j].properties["addr:city"]);
        //    console.log(zeleneZgradbe);     
        
      }
    });
  }
 
 
 
  function distance(lat1, lon1, lat2, lon2, unit) {
    if ((lat1 == lat2) && (lon1 == lon2)) {
  		return 0;
  	} else {
  	  var radlat1 = Math.PI * lat1/180;
  		var radlat2 = Math.PI * lat2/180;
  		var theta = lon1-lon2;
  		var radtheta = Math.PI * theta/180;
  		var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
  		if (dist > 1) {
  		  dist = 1;
  		}
  		dist = Math.acos(dist);
  		dist = dist * 180/Math.PI;
  		dist = dist * 60 * 1.1515;
  		if (unit=="K") { dist = dist * 1.609344 }
  		if (unit=="N") { dist = dist * 0.8684 }
  		return dist;
  	}
  }
  }
});